
package mx.curso.rest.lab2.service;

import java.util.ArrayList;
import java.util.List;

import mx.curso.rest.lab2.model.Departments;

/**
 * Generate Dummy data
 */
public class EntityManagerDummy {
    public static Departments getDummy(short id){
        return new Departments(id, "Dep "+id);        
    }
    public static Departments getRandom() {
        Double val = Math.random()*1000;
        
        return new Departments(val.shortValue(), "Dep "+val.shortValue());        
    }
    public static List<Departments> getRandomList(int max){
        List<Departments> l=new ArrayList<>();
        for(int i=0; i<max; i++) 
            l.add(getRandom());
        return l;
    }
}
