package mx.curso.rest.lab2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab2JaxrsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab2JaxrsApplication.class, args);
	}

}
