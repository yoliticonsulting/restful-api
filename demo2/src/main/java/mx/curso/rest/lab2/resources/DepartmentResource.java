package mx.curso.rest.lab2.resources;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import lombok.extern.slf4j.Slf4j;
import mx.curso.rest.lab2.model.Departments;
import mx.curso.rest.lab2.service.DepartmentFormBean;
import mx.curso.rest.lab2.service.EntityManagerDummy;

@Slf4j
@Path("departments")
public class DepartmentResource {
	/**
     * Returns list of departments
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Departments> findAllDepartments() {        
        return EntityManagerDummy.getRandomList(10);
    }

    /**
     * Creates Departments entity
     *
     * @param entity
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void createDepartment(Departments entity) {
        log.info("Create "+ entity.toString());
    }

    /**
     * Get total departments count
     *
     * @return
     */
    @GET
    @Path("count")
    @Produces("text/plain")
    public int countREST() {
        
        return 10; // dummy list

    }

    /**
     * Creates a department. This method demonstrates @FormParam
     *
     * @param departmentId
     * @param departmentName
     */
    @POST
    @Path("form")
    public void createDepartmnet(
            @FormParam("departmentId") short departmentId,
            @FormParam("departmentName") String departmentName) {
        Departments entity = new Departments();
        entity.setDepartmentId(departmentId);
        entity.setDepartmentName(departmentName);
        log.info("Create "+ entity.toString());
    }

    /**
     * Creates a department This method demonstrates @BeanParam
     *
     * @param deptBean
     */
    @POST
    @Path("form/bean")
    public void createDepartmnet(@BeanParam DepartmentFormBean deptBean) {
        createDepartmnet(deptBean.getDepartmentId(),
                deptBean.getDepartmentName());
    }

    /**
     * Modifies department
     *
     * @param id
     * @param entity
     */
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void editDepartment(@PathParam("id") Short id, Departments entity) {
        log.info(String.format("Edit %s by %s", entity.toString(),id));
    }

    /**
     * Deletes a department
     *
     * @param id
     */
    @DELETE
    @Path("{id}")
    public void removeDepartment(@PathParam("id") Short id) {
        log.info("Remove "+ id);

    }

    /**
     * Finds a department by name
     *
     * @param name
     * @return
     */
    @GET
    @Path("{name: [a-zA-Z][a-zA-Z_0-9]}")
    @Produces(MediaType.APPLICATION_JSON)
    public Departments findDepartmentByName(@PathParam("name") String name) {
        log.info("Find by "+name);
        Departments department = EntityManagerDummy.getRandom();
        return department;
    }

    /**
     * Finds a department by id
     *
     * @param id
     * @return
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Departments findDepartment(@PathParam("id") Short id) {
        return EntityManagerDummy.getDummy(id);
    }

    /**
     * Returns list of departments
     *
     * @param name
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("query")
    public List<Departments> findAllDepartmentsWithQueryParam(@QueryParam("name") String name) {        
        return EntityManagerDummy.getRandomList(10);
    }

    /**
     * Returns list of departments. This method demonstrates @MatrixParam
     *
     * @param name
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("matrix")
    public List<Departments> findAllDepartmentsWithMatrixParam(@MatrixParam("name") String name) {
        return EntityManagerDummy.getRandomList(10);
    }

    /**
     * Simple hello world API
     *
     * @return
     */
    @GET
    @Path("hello")
    @Produces(MediaType.APPLICATION_JSON)
    public String helloWorld() {
        return "hello world";
    }
}
