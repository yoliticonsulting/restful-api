package mx.curso.rest.lab2.config;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import mx.curso.rest.lab2.resources.DepartmentResource;

@ApplicationPath("webresources")
public class JaxrsConfig extends Application{
	@Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(DepartmentResource.class);
        return resources;
    }
}
