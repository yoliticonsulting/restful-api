package mx.curso.rest.lab2.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Department  entity
 */
@Data
@NoArgsConstructor
@XmlRootElement
public class Departments implements Serializable {
    private static final long serialVersionUID = 1L;
    private Short departmentId;
    @XmlElement(name = "departmentName")
    private String departmentName;  
    private Integer managerId;
    private Short locationId;
    
    public Departments(Short departmentId) {
        this.departmentId = departmentId;
    }

    public Departments(Short departmentId, String departmentName) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
    }

    
}
