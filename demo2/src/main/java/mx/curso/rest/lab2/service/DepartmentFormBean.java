package mx.curso.rest.lab2.service;

import java.io.Serializable;

import javax.ws.rs.FormParam;

import lombok.Data;

/**
 * DepartmentFormBean used as @BeanParam in DepartmentResource class
 */
@Data
public class DepartmentFormBean implements Serializable{
    
	private static final long serialVersionUID = 5368879365565637300L;

	@FormParam("departmentId") 
    private short departmentId;

    @FormParam("departmentName") 
    private String departmentName;

}
