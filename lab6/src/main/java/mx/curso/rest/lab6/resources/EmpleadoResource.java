/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.curso.rest.lab6.resources;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import mx.curso.rest.lab6.model.Empleado;

/**
 *
 * @author hugo
 */
@Path("empleados")
@Produces(MediaType.APPLICATION_JSON)
public class EmpleadoResource {
    
    @GET
    public List<Empleado> getAll() {
        final Empleado emp = new Empleado();
        emp.setEmpleadoId(1);
        emp.setEmpleadoName("Mi Nombre");
        return new ArrayList<Empleado>(){{add(emp);}};
    }
    
    @GET
    @Path("{id}")
    public Empleado get(@PathParam("id") int empleadoId) {
        final Empleado emp = new Empleado();
        emp.setEmpleadoId(empleadoId);
        emp.setEmpleadoName("Empleado #"+empleadoId);
        return emp;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Empleado emp){
        // Creado
        System.out.println("Creado "+emp.getEmpleadoName());
    }
}
