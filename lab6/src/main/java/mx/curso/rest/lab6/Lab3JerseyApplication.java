package mx.curso.rest.lab6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab3JerseyApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab3JerseyApplication.class, args);
	}

}
