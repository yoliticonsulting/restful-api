/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.curso.rest.lab6.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hugo
 */
@XmlRootElement
public class Departamento {
    private int departamentoId;

    @XmlElement(name = "id")
    public int getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(int departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getDepartamentoName() {
        return departamentoName;
    }

    public void setDepartamentoName(String departamentoName) {
        this.departamentoName = departamentoName;
    }
    private String departamentoName;
    
}
