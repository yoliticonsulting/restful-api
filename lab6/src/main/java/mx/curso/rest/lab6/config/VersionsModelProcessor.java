/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.curso.rest.lab6.config;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import org.glassfish.jersey.process.Inflector;
import org.glassfish.jersey.server.model.ModelProcessor;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceModel;

/**
 *
 * @author hugo
 */
@Provider
public class VersionsModelProcessor implements ModelProcessor {
    @Override
    public ResourceModel processResourceModel(ResourceModel rm, Configuration configuration) {
        ResourceModel.Builder newResourceModelBuilder = new ResourceModel.Builder(false);
        for (final Resource resource : rm.getResources()) {
            final Resource.Builder resourceBuilder = Resource.builder(resource);
            resourceBuilder.addChildResource("version") // Agregar otro Subresource
                    .addMethod(HttpMethod.GET)
                    .handledBy(new Inflector<ContainerRequestContext, String>() {
                        @Override
                        public String apply(ContainerRequestContext data) {
                            return "version : 1.0";
                        }
                    })
                    .produces(MediaType.TEXT_PLAIN)
                    .extended(true);
            newResourceModelBuilder.addResource(resourceBuilder.build());
        }        
        return newResourceModelBuilder.build();
    }
    @Override
    public ResourceModel processSubResource(ResourceModel subResourceModel, Configuration configuration) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
