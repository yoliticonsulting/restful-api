/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.curso.rest.lab6.resources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author hugo
 */
@Path("imagen")
public class ImagenResource {

    //** Cargar imagen
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void uploadImage(@Context HttpServletRequest request,
            @FormDataParam("file") InputStream in,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws Exception {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            int size = request.getContentLength();
            byte[] buffer = new byte[size];
            int len = 0;
            while ((len = in.read(buffer, 0, 10240)) != -1) {
                bos.write(buffer, 0, len);
            }
            byte[] imgBytes = bos.toByteArray();

            // System.out.write(imgBytes);
            System.out.println(fileDetail.getName());
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getImage() throws Exception {
        StreamingOutput outputImage = new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {

                byte[] buf = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
                output.write(buf);
                output.flush();
            }
        };
        ResponseBuilder response = Response.ok((Object) outputImage);
        response.header("Content-Disposition", "inline;filename=image.jpeg");
        return response.build();
    }
}
