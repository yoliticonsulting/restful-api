/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.curso.rest.lab6.model;

import java.io.Serializable;
import java.util.List;
import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Data;
import org.glassfish.jersey.linking.Binding;
import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;

/**
 *
 * @author hugo
 */
@Data
// @XmlRootElement
public class Empleado implements Serializable {

    private int empleadoId;
    private String empleadoName;
/*
    @XmlElement(name = "empId")
    public int getEmpleadoId() {
        return empleadoId;
    }

    public void setEmpleadoId(int empleadoId) {
        this.empleadoId = empleadoId;
    }

    public String getEmpleadoName() {
        return empleadoName;
    }

    public void setEmpleadoName(String empleadoName) {
        this.empleadoName = empleadoName;
    }
    */
    @InjectLinks({
        @InjectLink(
                value = "{id}/trabajaen",
                style = InjectLink.Style.ABSOLUTE_PATH,
                bindings = @Binding(name = "id", value = "${instance.empleadoId}"),
                rel = "trabajaen"
        )
    })
    @XmlJavaTypeAdapter(Link.JaxbAdapter.class)
    @XmlElement(name = "link")
    private List<Link> links;

    /*public List<Link> getLinks() {
        return links;
    }
*/

}
