package mx.curso.rest.lab4.config;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import mx.curso.rest.lab4.exceptions.DepartmentNotFoundExceptionMapper;
import mx.curso.rest.lab4.resources.DepartamentoResource;
import mx.curso.rest.lab4.resources.EmpleadoResource;
import mx.curso.rest.lab4.resources.HRResource;

@ApplicationPath("api")
public class JaxrsApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(EmpleadoResource.class);
        resources.add(HRResource.class);
        resources.add(DepartamentoResource.class);
        resources.add(DepartmentNotFoundExceptionMapper.class);
        return resources;
    }
}
