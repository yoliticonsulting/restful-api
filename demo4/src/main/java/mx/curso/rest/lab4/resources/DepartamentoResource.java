package mx.curso.rest.lab4.resources;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Digits;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import mx.curso.rest.lab4.exceptions.DepartmentNotFoundBusinessException;
import mx.curso.rest.lab4.exceptions.DepartmentNotFoundWebAppException;
import mx.curso.rest.lab4.model.Departamento;
import mx.curso.rest.lab4.validator.ValidDepartment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Path("departamentos")
@Produces(MediaType.APPLICATION_JSON)
public class DepartamentoResource {

	@GET
	public List<Departamento> getAll() {
		final Departamento emp = new Departamento();
		emp.setDepartamentoId(1);
		emp.setDepartamentoName("Departamento 1");

		return new ArrayList<Departamento>() {
			{
				add(emp);
			}
		};
	}

	@GET
	@Path("{id}")
	public Departamento get(@PathParam("id") String empleadoId) {
		final Departamento emp = new Departamento();
		emp.setDepartamentoId(Integer.valueOf(empleadoId));
		emp.setDepartamentoName("Departamento #" + empleadoId);
		return emp;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void create(@ValidDepartment Departamento dept) {
		if(dept.getDepartamentoId()==10) {
			throw new DepartmentNotFoundWebAppException("Se validó id 10");
		}
	}

	@DELETE
	@Path("{id}")
	public Response remove(@PathParam("id") Short id) throws DepartmentNotFoundBusinessException {
		if (id == (short) 1) {
			throw new DepartmentNotFoundBusinessException("Id no válido");
		}else if(id == (short)10) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		return Response.noContent().build();
	}
	
	
    @Path("{id}/manager")
    @Produces(MediaType.APPLICATION_JSON)
    public EmpleadoResource findManagerForDepartment(@PathParam("id") int deptId) {
	
		return new EmpleadoResource(deptId);
	}
}
