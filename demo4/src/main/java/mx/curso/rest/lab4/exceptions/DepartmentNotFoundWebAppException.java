package mx.curso.rest.lab4.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class DepartmentNotFoundWebAppException extends WebApplicationException {
	private static final long serialVersionUID = 1L;

	/**
     * Create a HTTP 404 (Not Found) exception.
     */
    public DepartmentNotFoundWebAppException() {
        super(Response.Status.NOT_FOUND);
    }

    /**
     * Create a HTTP 404 (Not Found) exception.
     *
     * @param message
     */
    public DepartmentNotFoundWebAppException(String message) {
        super(Response.status(Status.NOT_FOUND).entity(message).type(MediaType.TEXT_PLAIN).build());
    }
}