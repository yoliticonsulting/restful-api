package mx.curso.rest.lab4.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = {ValidDepartmentValidator.class})
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ValidDepartment {

    //The message to return when the instance of DepartmentValidator
    //fails the validation.
    String message() default "Validacion de departamenteo";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
