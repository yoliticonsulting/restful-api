package mx.curso.rest.lab4.exceptions;

public class DepartmentNotFoundBusinessException extends Exception {
	private static final long serialVersionUID = 3900364531631024941L;

	public DepartmentNotFoundBusinessException(String message) {
		super(message);
	}

	public DepartmentNotFoundBusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public DepartmentNotFoundBusinessException(Throwable cause) {
		super(cause);
	}

}
