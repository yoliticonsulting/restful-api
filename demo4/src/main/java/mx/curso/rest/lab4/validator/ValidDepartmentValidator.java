package mx.curso.rest.lab4.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import lombok.extern.slf4j.Slf4j;
import mx.curso.rest.lab4.model.Departamento;

@Slf4j
public class ValidDepartmentValidator implements ConstraintValidator<ValidDepartment, Departamento> {


    @Override
    public void initialize(ValidDepartment constraintAnnotation) {
    }

    @Override
    public boolean isValid(Departamento department, ConstraintValidatorContext context) {        
        if (isDeptExistsForLoc(department.getDepartamentoId(), department.getDepartamentoName())) {
            return true;
        }

        return false;
    }

    private boolean isDeptExistsForLoc(int deptId, String deptName) {
        return deptId==1;
    }

}