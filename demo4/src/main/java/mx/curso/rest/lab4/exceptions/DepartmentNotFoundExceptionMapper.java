package mx.curso.rest.lab4.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Provider
public class DepartmentNotFoundExceptionMapper implements ExceptionMapper<DepartmentNotFoundBusinessException> {

    public DepartmentNotFoundExceptionMapper() {
        // log.info("-----DepartmentNotFoundExceptionMapper-----------");
    }

    @Override
    public Response toResponse(DepartmentNotFoundBusinessException exception) {
        return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
    }

}