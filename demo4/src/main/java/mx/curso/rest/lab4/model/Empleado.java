package mx.curso.rest.lab4.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class Empleado implements Serializable{
	private int empleadoId;
	private String empleadoName;
}
