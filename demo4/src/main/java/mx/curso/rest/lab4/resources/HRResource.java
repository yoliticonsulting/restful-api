package mx.curso.rest.lab4.resources;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("hr")
@Produces(MediaType.APPLICATION_JSON)
public class HRResource {

	@Path("departamentos")
	public Class<DepartamentoResource> showDepartments() {
		return DepartamentoResource.class;
	}

}
