package mx.curso.rest.lab4.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

import mx.curso.rest.lab4.model.Empleado;

// @Path("empleados")
@Slf4j
@Produces({MediaType.APPLICATION_JSON})
public class EmpleadoResource {

    private int empleadoId=0;

    public EmpleadoResource() {
    }

    public EmpleadoResource(int empleadoId) {
        this.empleadoId = empleadoId;
    }

    @GET
    public Empleado get() {
        log.debug("getting Empleado");
        final Empleado emp = new Empleado();
        emp.setEmpleadoId(empleadoId);
        emp.setEmpleadoName("Empleado #" + empleadoId);
        return emp;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Empleado emp) {
    }
}
