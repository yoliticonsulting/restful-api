package mx.curso.rest.lab4.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class Departamento implements Serializable{
	private int departamentoId;
	private String departamentoName;
}
