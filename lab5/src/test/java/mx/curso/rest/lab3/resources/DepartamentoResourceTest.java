package mx.curso.rest.lab3.resources;

import java.util.List;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Hugo Estrada
 */
@RunWith(JUnit4ClassRunner.class)
public class DepartamentoResourceTest extends JerseyTest {

    public DepartamentoResourceTest() {
        

    }

    @Override
    protected Application configure() {
        ResourceConfig resourceConfig = new ResourceConfig(EmpleadoResource.class);
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.refresh();
        resourceConfig.property("contextConfig", context);
        return resourceConfig;
    }

    /**
     * Test of worksIn method, of class DepartamentoResource.
     */
    @Test
    public void listaDeEmpleadosTest() {
        Response response = target("/empleados").request(MediaType.APPLICATION_JSON).get();

        assertEquals("Respuesta 200-OK", 200, response.getStatus());
        assertTrue("Regresa una lista", response.getEntity() instanceof List);
    }

}
