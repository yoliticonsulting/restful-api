package mx.curso.rest.lab3.resources;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import mx.curso.rest.lab3.ext.CSVMessageBodyReader;
import mx.curso.rest.lab3.ext.CSVMessageBodyWriter;
import org.glassfish.jersey.server.ResourceConfig;

import org.springframework.stereotype.Component;

@Component
@ApplicationPath("api")
public class JaxrsApplication extends ResourceConfig {

    public JaxrsApplication() {
        packages("mx.curso.rest.lab3");
        register(CSVMessageBodyReader.class);
        register(CSVMessageBodyWriter.class);
    }
	
	/*public Set<Class<?>> getClasses() {
		Set<Class<?>> resources = new java.util.HashSet<>();
		resources.add(EmpleadoResource.class);
		resources.add(DepartamentoResource.class);
		//resources.add(LogRequestFilter.class);
		//resources.add(LogResponseFilter.class);
		return resources;
	}*/
    
}