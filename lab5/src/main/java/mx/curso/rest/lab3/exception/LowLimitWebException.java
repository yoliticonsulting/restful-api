package mx.curso.rest.lab3.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class LowLimitWebException extends WebApplicationException{
    public LowLimitWebException() {
        super(Response.Status.BAD_REQUEST);
    }
    public LowLimitWebException(String msg){
        super(Response.status(Response.Status.BAD_REQUEST)
             .entity(msg).type(MediaType.TEXT_PLAIN).build());
    }
}
