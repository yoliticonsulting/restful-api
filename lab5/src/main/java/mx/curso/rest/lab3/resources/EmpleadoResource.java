package mx.curso.rest.lab3.resources;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Max;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;

import mx.curso.rest.lab3.exception.LowLimitWebException;
import mx.curso.rest.lab3.model.Empleado;

@Slf4j
@Path("empleados")
@Produces(MediaType.APPLICATION_JSON)
public class EmpleadoResource {

    @GET
    @Produces({MediaType.APPLICATION_JSON, "application/csv" })
    public List<Empleado> getAll() {
        final Empleado emp = new Empleado();
        emp.setEmpleadoId((short)1);
        emp.setEmpleadoName("Mi Nombre");

        return new ArrayList<Empleado>() {
            {
                add(emp);
            }
        };
    }

    @GET
    @Path("{id}")
    public Empleado get(@PathParam("id") short empleadoId) {
        if (empleadoId <= 1) {
            throw new LowLimitWebException("El valor es muy pequeño:" + empleadoId);
        }
        final Empleado emp = new Empleado();
        emp.setEmpleadoId(empleadoId);
        emp.setEmpleadoName("Empleado #" + empleadoId);
        return emp;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Empleado emp) {
        log.debug(emp.toString());
    }
    @POST
    @Consumes("application/csv")
    public void create(List<Empleado> emp) {
        log.debug(emp.toString());
    }

    @GET
    @Path("{id}/trabajaen")
    public DepartamentoResource works(@PathParam("id") @Max(10) int id) {
        return new DepartamentoResource();
    }
}
