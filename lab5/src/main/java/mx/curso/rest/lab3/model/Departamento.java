package mx.curso.rest.lab3.model;

import lombok.Data;

@Data
public class Departamento {
	private int departamentoId;
	private String departamentoName;
}
