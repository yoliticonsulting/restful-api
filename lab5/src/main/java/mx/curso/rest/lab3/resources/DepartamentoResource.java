package mx.curso.rest.lab3.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.curso.rest.lab3.model.Departamento;

@Slf4j
@Path("departamentos")
@Produces(MediaType.APPLICATION_JSON)
public class DepartamentoResource {

    @GET
    public Response worksIn() {
        final Departamento dept = new Departamento();
        dept.setDepartamentoId(9);
        dept.setDepartamentoName("Trabaja en #" + 9);
        return Response.ok(dept).header("XXX-My-header", "#9").build();
    }
}
