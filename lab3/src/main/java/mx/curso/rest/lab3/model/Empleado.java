package mx.curso.rest.lab3.model;

import lombok.Data;

@Data
public class Empleado {
	private int empleadoId;
	private String empleadoName;
}
