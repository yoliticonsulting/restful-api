package mx.curso.rest.lab3.resources;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("api")
public class JaxrsApplication extends ResourceConfig {

    public JaxrsApplication() {
        packages("mx.curso.rest.lab3.resources");
    }
	
}