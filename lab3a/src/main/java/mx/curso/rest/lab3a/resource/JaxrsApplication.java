package mx.curso.rest.lab3a.resource;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("api")
public class JaxrsApplication extends ResourceConfig {

    public JaxrsApplication() {
        
        packages("mx.curso.rest.lab3a");
    }
}
