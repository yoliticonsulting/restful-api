/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.curso.rest.lab3a.resource;

/**
 *
 * @author Hugo Estrada
 */
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import mx.curso.rest.lab3a.model.Departamento;

@Path("departamentos")
@Produces(MediaType.APPLICATION_JSON)
public class DepartamentoResource {

    @GET
    public Response worksIn() {
        final Departamento dept = new Departamento();
        dept.setDepartamentoId(9);
        dept.setDepartamentoName("Trabaja en #" + 9);
        return Response.ok(dept).header("XXX-My-header", "#9").build();
    }
}
