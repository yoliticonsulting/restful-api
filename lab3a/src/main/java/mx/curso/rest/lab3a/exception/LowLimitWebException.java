package mx.curso.rest.lab3a.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Hugo Estrada
 */
public class LowLimitWebException extends WebApplicationException {

    public LowLimitWebException() {
        super(Response.Status.TOO_MANY_REQUESTS);
    }

    public LowLimitWebException(String msg) {
        super(Response.status(Response.Status.TOO_MANY_REQUESTS)
                .entity(msg).type(MediaType.TEXT_PLAIN).build());
    }
}
