package mx.curso.rest.lab3a.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class Departamento implements Serializable{
	private int departamentoId;
	private String departamentoName;
}
