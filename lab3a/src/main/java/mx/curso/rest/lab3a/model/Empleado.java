/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.curso.rest.lab3a.model;

import lombok.Data;

/**
 *
 * @author Hugo Estrada
 */
@Data
public class Empleado {
    private int empleadoId;
    private String empleadoName;
}
