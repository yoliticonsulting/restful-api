/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.curso.rest.lab3a.resource;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Max;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import mx.curso.rest.lab3a.exception.LowLimitWebException;
import mx.curso.rest.lab3a.model.Empleado;

/**
 *
 * @author Hugo Estrada
 */
@Path("empleados")
@Produces(MediaType.APPLICATION_JSON)
public class EmpleadoResource {

    @GET
    public List<Empleado> getAll() {
        final Empleado emp = new Empleado();
        emp.setEmpleadoId(1);
        emp.setEmpleadoName("Mi Nombre");
        return new ArrayList<Empleado>() {
            {
                add(emp);
            }
        };
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") @Max(10) int empleadoId) {
        if (empleadoId <= 1) {
            throw new LowLimitWebException("El valor es muy pequeño:" + empleadoId);
        }

        final Empleado emp = new Empleado();
        emp.setEmpleadoId(empleadoId);
        emp.setEmpleadoName("Empleado #" + empleadoId);

        Response.ResponseBuilder response = Response.ok(emp).type(MediaType.APPLICATION_JSON);
        if (empleadoId == 3) {
            CacheControl cc = new CacheControl();
            cc.setMaxAge(86400); // Age en segundos 
            cc.setPrivate(true); // Aplica solo para el cliente que invoca 
            response.cacheControl(cc); 
        }
        
        return response.build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Empleado emp) {
    }

    @Path("{id}/trabajaen")
    public DepartamentoResource works(@PathParam("id") @Max(10) int id) {
        return new DepartamentoResource();
    }
}
