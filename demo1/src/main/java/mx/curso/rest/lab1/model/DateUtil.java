
package mx.curso.rest.lab1.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Common utility class for date relates features
 */
public abstract class DateUtil {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Returns the current time as a String.
     * @param date
     * @return 
     */
    public static String getDate(Date date) {
        return SIMPLE_DATE_FORMAT.format(date);
    }

    /**
     * Returns the current time as a String.
     * @param date
     * @return 
     */
    public static Date getDate(String date) {

        try {
            return SIMPLE_DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
