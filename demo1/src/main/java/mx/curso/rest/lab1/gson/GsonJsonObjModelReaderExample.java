
package mx.curso.rest.lab1.gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import mx.curso.rest.lab1.model.Employee;

/**
 * This class demonstrates object model reader APIs in Gson
 *
 */
public class GsonJsonObjModelReaderExample {

    private static final Logger logger = Logger.getLogger(GsonJsonObjModelReaderExample.class.getName());

    public static void main(String arg[]) throws IOException {
        logger.setLevel(Level.INFO);
        //Exercise buildEmployeeList(String fileName) method
        List<Employee> employees = new GsonJsonObjModelReaderExample().buildEmployeeList("/emp-array.json");
        logger.log(Level.INFO, employees.toString());
    }

    /**
     * Reads JSON representation of employee array present in the input file and
     * converts the contents in to List of employee objects
     *
     * @param fileName
     * @return
     * @throws IOException
     */
    public List<Employee> buildEmployeeList(String fileName) throws IOException {
        // Override default date format and get Gson object
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        //Define the ArrayList<Employee> to store the JSON representation from input file
        Type listType = new TypeToken<ArrayList<Employee>>() {
        }.getType();
        InputStream inputStream = GsonJsonObjModelReaderExample.class.getResourceAsStream(fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        //Converts JSON representation of employee objects in to List<Employee> 
        List<Employee> employees = gson.fromJson(reader, listType);
        //Following is just to exercise toJson method, not relevant otherwise
        //String jsonEmp = gson.toJson(employees);

        return employees;
    }
}
